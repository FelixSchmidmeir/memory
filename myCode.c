////////////////////////////////////
//Name 1:     XXXXXXXXX XXXXXXXXX //
//Matrikel 1: XXXXXXX             //
//Note 1:     JA/NEIN             //
//Schein 1:   JA/NEIN             //
//Name 2:     XXXXXXXXX XXXXXXXXX //
//Matrikel 2: XXXXXXX             //
//Note 2:     JA/NEIN             //
//Schein 2:   JA/NEIN             //
////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <time.h>
#include <string.h>

//Struct welches eine Spielkarte repraesentiert
struct card{
	bool visible;
	char color;
	char sign;
};

//Funktion um das Terminal zu leeren
void clear(){
	printf("\033[H\033[J");
}

//Funktion um den input buffer des Terminals zu leeren
void clear_input_buffer(){
	while (getchar() != '\n');
}

//Funktion um vom Spieler zu erfahren ob er gegen einen Gegner spielen moechte oder nicht
//gibt true(1) fuer y und false(0) fuer n zurueck
bool get_game_mode(){
	printf("\nMoechtest du gegen einen Gegner spielen? Gebe y fuer ja oder n fuer nein ein\n");
	
	char input;
	
	scanf("%c",&input);
	clear_input_buffer();
	if(input == 'y'){
		return true;
	}else if(input == 'n'){
		return false;
	}	
	
	printf("Deine Eingabe war inkorrekt versuche es noch einmal\n");
	
	return get_game_mode();
}

//Funktion um vom Spieler zu erfahren ob er mit 20 oder 36 Karten spielen moechte
int choose_size(){
	printf("\nDas Spielfeld ist in den groeßen 4x4 oder 6x6 verfuegbar\nbitte gib 4 oder 6 ein um zu waehlen\n");
	
	int input;
	
	scanf("%d", &input);
	if( input == 4 || input == 6 ){
		return input;
	}	
	
	clear_input_buffer();
	printf("Deine Eingabe war inkorrekt bitte wiederhohle diese\n");
	
	return choose_size();
}

//Funktion um vom Spieler den index des Feldes zu erhalten welches er aufdecken will
int get_input(int line_length, struct card * ptr){
	int index;
	
	do{
		printf("\nWähle bitte ein Feld, welches du aufdecken moechtest.\n");
		char col;
		do{
			printf("Bitte gib den Großbuchstaben fuer die Spalte ein, welche du waehlen moechtest\n");
			clear_input_buffer();
			scanf("%c", &col);
			//check ob die Spalte existiert
		}while(col < 'A' || col > ('A'+line_length-1));
		
		int row;
		
		do{
			printf("Bitte gib die Zahl für die Reihe ein, welche du waehlen moechtest\n");
			clear_input_buffer();
			scanf("%d", &row);
			//check ob die Reihe existiert
		}while(row < 0 || row > line_length-1);
		
		index = (int)col-65+row*line_length;
		
	//fuer den Fall, dass das gewaehlte Feld schon aufgedeckt ist
	}while(ptr[index].visible);
	
	//array index von ausgewaehlten Feld zurueck geben
	return index;	
}

//Funktion um Spielfeld im Terminal auszugeben
void print_field(struct card * ptr, int row_length){
	clear();
	printf("  ");
	for(int i = 0; i < row_length; i++){
		printf("%c ", (char)i+65);
	}
	for(int i = 0; i < row_length; i++){
		printf("\n%d ", i);
		for(int index = 0; index < row_length; index++){
			struct card element = ptr[i*row_length + index];
			if(element.visible){
				printf("%c ", element.sign);
			}else{
				printf("x ");
			}
		}
	}
	printf("\n\n");
}

//Funktion um gewaehlte Karten aufzudecken und durch einen großen Buchstaben zu ersetzen
void turn_card(struct card ** array, int array_index, int line_length){
	(*array)[array_index].visible = true;
	(*array)[array_index].sign = (*array)[array_index].sign-32;
	print_field(*array, line_length);
}

//Funktion um die großen Buchstaben wieder durch kleine zu ersetzen
void down_size_sign(struct card ** array, int input1, int input2){
	(*array)[input1].sign = (*array)[input1].sign+32;
	(*array)[input2].sign = (*array)[input2].sign+32;
}

//Funktion die Kartenpaare die nicht zusammen passen wieder auf nicht sichtbar stellt
void no_pair(struct card ** array, int input1, int input2){
	(*array)[input1].visible = false;
	(*array)[input2].visible = false;
}

//Funktion um die Karte zu waehlen welche der Gegenspieler aufdeckt
//es werden nur Karten aufgedeckt welche noch nicht visible sind
int bot_move(struct card * array, int array_length){
	int index;
	 do{
		index = rand() % array_length;
	 }while(array[index].visible);
	
	return index;
}

//Funktion um festzustellen ob das Spiel vorbei ist oder nicht gibt true zurück falls spiel zu ende ist
//Das Spiel ist nicht zu Ende falls mindestens eine Karte noch nicht visible ist
bool check_if_finished(int array_length, struct card * ptr){
	for(int i = 0; i < array_length; i++){
		if( !ptr[i].visible){
			return false;
		}
	}
	return true;
}


//struct und Methoden fuer den highscore

struct score{
	char name[20];
	int size;
	int rounds;
	bool mode;
	struct score * next;
};

//Ausgabe des aktuellen Highscores
void ausgeben(struct score * root_addr, int line_length, bool game_mode ){
	printf("Highscore fuer Spielfeld groeße %dx%d", line_length, line_length);
	if(game_mode){
		printf(" mit einem Gegenspieler\n");
	}else{
		printf(" ohnen einen Gegenspieler\n");
	}
	
	//Liste wird komplett durchlaufen es werde aber nur die Elemente ausgegeben, welche die passende Feldgroeße und den richtigen Spielmodus haben
	int index = 1;
	while(root_addr != NULL){
		if(root_addr->mode == game_mode && root_addr->size == line_length){
			printf("%d. %d %s\n", index, root_addr->rounds, root_addr->name);
			index = index + 1;
		}
		root_addr = root_addr->next;
	}
	printf("\n");
}

//Funktien,die die Elemente anhand von rounds absteigend in die Liste sortiert
void sorted_insert( struct score ** root_addr, struct score * new_element){
	//speicherplatz fuer das neue Element erzeugen
	struct score * tmp = malloc(sizeof(struct score));
	tmp->size = new_element->size;
	tmp->rounds = new_element->rounds;
	tmp->mode = new_element->mode;
	strcpy(tmp->name, new_element->name);
	
	struct score *lauf = *root_addr;
	
	//falls root auf Null zeigt oder das Element die aktuel hoechste Anzahl an rounds hat
	if(lauf == NULL || (lauf->rounds > tmp->rounds)){
		tmp->next = lauf;
		*root_addr = tmp;
	}else{
		//an passende Stelle iterieren um das Element einzufuegen
		while((lauf->next != NULL) && (lauf->next->rounds <= tmp->rounds) ){
			lauf = lauf->next;
		}
		
		tmp->next = lauf->next;
		lauf->next = tmp;
	}
}

//Funktion die den alten Highscore aus einem txt File in eine Liste einliest, durch das neue Element ergaenzt
//und anschließend den aktualisierten Highscore wieder in das File schreibt
void highscore ( char name[], int rounds, bool game_mode, int line_length ){
	struct score * root = NULL;
	
	//txt File zum einlesen des Highscores oeffnen	
	FILE * handle = fopen("highscore.txt", "r");
	
	if(handle == NULL){
		exit(1);
	}	
	
	struct score element;

	//Elemente einlesen und in Liste einfuegen
	while(fread(&element, sizeof(struct score), 1, handle)){
		sorted_insert(&root, &element);
	}
	
	fclose(handle);
		
	//neues Highscore Element erzeugen und einfuegen
	struct score * new_element;
	new_element = malloc(sizeof(struct score));
	new_element->size = line_length;
	new_element->rounds = rounds;
	new_element->mode = game_mode;
	strcpy(new_element->name, name);
	new_element->next = NULL;
	
	sorted_insert(&root, new_element);
	
	//aktuellen Highscore ausgeben
	ausgeben(root, line_length, game_mode);
	
	//File zum schreiben des aktuellen Highscores oeffnen
	FILE * handle2 = fopen("highscore.txt", "w");
	
	if(handle2 == NULL){
		exit(1);
	}
	
	while(root != NULL){
		fwrite(root, sizeof(struct score), 1, handle2);
		root = root->next;
	}
	fclose(handle2);
}



int main(void){
	
	
	//Spieler abfragen ob er einen Gegenspieler haben moechte
	bool game_mode = get_game_mode();
	
	//Spieler abfragen wie groß das Spielfeld sein soll
	int line_length = choose_size();
	int array_length = line_length * line_length;
	
	//dynamisches array als Spielfeld erzeugen
	struct card * array = malloc(array_length * sizeof(struct card));
	
	//alle Karten zu Beginn des Spiels auf nicht sichtbar stellen
	for( int i = 0; i < array_length; i++){
		array[i].visible = false;
		array[i].sign = 'A';
	}
	
	srand(time(NULL));
	
	//Buchstaben Paare zufällig über das Spielfeld verteilen
	//es werden halb so viele verschiedene Buchstaben benoetigt wie das Array Lang ist
	for( int i = 0; i < array_length/2; i++){
		//wird für jeden Buchstaben zweimal ausgefuehrt
		for(int index = 0; index < 2; index++){
	
			//Falls das zufaellig gewaehlte Feld bereits belegt ist wiederholt sich die Schleife
			int array_index;
			do{
				array_index = rand() % array_length;
				
			}while(array[array_index].sign != 'A');		
			
			array[array_index].sign = (char) i+97;
		}
	}
	
	
	
	int rounds = 0;
	
	//Das Spiel beginnt
	do{
		rounds++;
		print_field(array, line_length);
		
		//Karten durch den Spieler aufdecken
		int input1 = get_input(line_length, array);
		turn_card(&array, input1, line_length);
		
		int input2 = get_input(line_length, array);
		turn_card(&array, input2, line_length);
		
		//falls die aufgedeckten Karten nicht zu einander passen werden diese wieder auf visible = false gesetzt
		if(array[input1].sign != array[input2].sign){
			printf("Deine Eingabe war leider falsch\n");
			no_pair(&array, input1, input2);
			sleep(3);
		}else{
			printf("Deine Eingabe war richtig !!! \n");
		}
		
		//Zeichen wieder auf klein setzten
		down_size_sign(&array, input1, input2);
		
		//falls der Gegenspieler aktiviert ist und das Spiel noch nicht beendet ist, ist dieser jetzt am Zug
		if(game_mode && !check_if_finished(array_length, array)){
			printf("\nDer Gegenspieler ist nun dran\n");
			sleep(2);
			int move1 = bot_move(array, array_length);
			turn_card(&array, move1, line_length);
			sleep(2);
			int move2 = bot_move(array, array_length);
			turn_card(&array, move2, line_length);
			sleep(3);
			
			if(array[move1].sign != array[move2].sign){
				no_pair(&array, move1, move2);
			}
			down_size_sign(&array, move1, move2);
		}		
		
	}while(!check_if_finished(array_length, array));
	
	free(array);
	
	printf("Bitte gebe deinen Namen fuer den Highscore ein\n");
	
	//es werden bewusst keine Zeichen abgefangen
	//die laenge des Namens ist auf maximal 20 Zeichen beschraenkt und er darf kein Lehrzeichen enthalten
	//falls jemand Sonderzeichen oder Zahlen in seinem Namen haben will ist das erlaubt
	
	char name[20];
	scanf("%s", name);
	
	//alter Highscore wird aus einem txt File in eine Liste eingelesen, durch das neue Element ergaenzt
	//anschließend wird der aktualisierte Highscore wieder in das File geschrieben
	//Ich hatte dies Urspruenglich in eine eigene Funtkion ausgelagert,da der Code jedoch nur einmal ausgefuehrt wird ist dies nicht noetig

	struct score * root = NULL;
	
	//txt File zum einlesen des Highscores oeffnen	
	FILE * handle = fopen("highscore.txt", "r");
	
	if(handle == NULL){
		exit(1);
	}	
	
	struct score element;

	//Elemente einlesen und in Liste einfuegen
	while(fread(&element, sizeof(struct score), 1, handle)){
		sorted_insert(&root, &element);
	}
	
	fclose(handle);
		
	//neues Highscore Element erzeugen und einfuegen
	struct score * new_element;
	new_element = malloc(sizeof(struct score));
	new_element->size = line_length;
	new_element->rounds = rounds;
	new_element->mode = game_mode;
	strcpy(new_element->name, name);
	new_element->next = NULL;
	
	sorted_insert(&root, new_element);
	
	//aktuellen Highscore ausgeben
	ausgeben(root, line_length, game_mode);
	
	//File zum schreiben des aktuellen Highscores oeffnen
	FILE * handle2 = fopen("highscore.txt", "w");
	
	if(handle2 == NULL){
		exit(1);
	}
	
	while(root != NULL){
		fwrite(root, sizeof(struct score), 1, handle2);
		root = root->next;
	}
	fclose(handle2);

	printf("das spiel ist nach %d Runden zu Ende\n", rounds);
}